<?php

//global variables : links to storage folder in aws s3
return [

	'ss3Link' => 'https://'. env('AWS_BUCKET') .'.s3.amazonaws.com/',
    'productsPath' => 'asset/photos/1/Products',
    'categoryPath' => 'asset/photos/1/Category',
    'blogPath' => 'asset/photos/1/Blog',
    'bannerPath' => 'asset/photos/1/Banner',
    'userPath' => 'asset/photos/1/users',
    'thumhsPath' => 'asset/photos/1/thumbs',
];
