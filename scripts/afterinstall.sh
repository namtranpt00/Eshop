#!/bin/bash
cd /var/www/html/Eshop
docker-compose down
sleep 5
docker-compose up -d
sleep 10
docker-compose exec php bash
php artisan cache:clear
php artisan config:clear
php artisan migrate --force
composer dump-autoload --ignore-platform-reqs
exit
docker-compose exec php php artisan migrate
chmod o+w ./storage/ -R
echo "docker-compose exec php php artisan migrate" | at -M now + 1 minute;
echo "service codedeploy-agent restart" | at -M now + 2 minute;


